﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pinga
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime nextPingTime = DateTime.Now;
            DateTime stopTime = DateTime.Now.AddHours(96);
            while (DateTime.Now<stopTime)
            {
                if (DateTime.Now > nextPingTime)
                {
                    nextPingTime = nextPingTime.AddSeconds(10);
                    ping();
                }
                Thread.Sleep(100);
            }
        }

        private static void ping()
        {
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();

            // Use the default Ttl value which is 128,
            // but change the fragmentation behavior.
            options.DontFragment = true;

            // Create a buffer of 32 bytes of data to be transmitted.
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 2000;
            PingReply reply = pingSender.Send("10.7.5.155", timeout, buffer, options);
            Console.WriteLine(DateTime.Now + " " + reply.Status + " " + reply.RoundtripTime);
        }
    }
}
